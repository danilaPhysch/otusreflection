﻿// See https://aka.ms/new-console-template for more information

using System.Diagnostics;
using System.Text;
using System.Text.Json;

internal class Program
{
    public static void Main(string[] args)
    {
        Serialization();
        Deserialization();
    }

    private static void Serialization()
    {
        var iterations = 100000;
        var f = new F { I1 = 1, I2 = 2, I3 = 3, I4 = 4, I5 = 5 };

        var stopwatch = new Stopwatch();
        stopwatch.Start();

        for (var i = 0; i < iterations; i++)
        {
            SerializeGeneric(f);
        }

        stopwatch.Stop();

        Console.WriteLine($"Generic: {stopwatch.Elapsed}");

        stopwatch.Restart();

        for (var i = 0; i < iterations; i++)
        {
            SerializeObject(f);
        }

        stopwatch.Stop();

        Console.WriteLine($"Object: {stopwatch.Elapsed}");

        stopwatch.Restart();

        for (var i = 0; i < iterations; i++)
        {
            JsonSerializer.Serialize(f);
        }

        stopwatch.Stop();

        Console.WriteLine($"JsonSerializer: {stopwatch.Elapsed}");
    }

    private static void Deserialization()
    {
        using var fileStream = new StreamReader(@"..\..\..\file.ini");
        var str = fileStream.ReadToEnd();

        var iterations = 100000;

        var stopwatch = new Stopwatch();
        stopwatch.Start();

        for (var i = 0; i < iterations; i++)
        {
            DeserializeIni<F>(str);
        }

        stopwatch.Stop();

        Console.WriteLine($"Deserialization: {stopwatch.Elapsed}");
    }

    private static string SerializeGeneric<T>(T input)
    {
        var output = new StringBuilder();

        var type = typeof(T);

        foreach (var fieldInfo in type.GetProperties())
            output.Append(fieldInfo.GetValue(input));

        return output.ToString();
    }

    private static string SerializeObject(object input)
    {
        var output = new StringBuilder();

        var type = input.GetType();

        foreach (var fieldInfo in type.GetProperties())
            output.Append(fieldInfo.GetValue(input));

        return output.ToString();
    }

    private static T DeserializeIni<T>(string input) where T : new()
    {
        var type = typeof(T);

        var output = new T();

        var properties = type.GetProperties();
        var fields = type.GetFields();

        var values = input.Split('\r', '\n').Where(x => x != string.Empty).ToDictionary(x => x.Split("=")[0], x => x.Split("=")[1]);

        foreach (var property in properties)
        {
            if (values.TryGetValue(property.Name, out var value))
            {
                property.SetValue(output, int.Parse(value));
            }
        }

        foreach (var field in fields)
        {
            if (values.TryGetValue(field.Name, out var value))
            {
                field.SetValue(output, int.Parse(value));
            }
        }

        return output;
    }
}

internal class F
{
    public int I1 { get; set; }
    public int I2 { get; set; }
    public int I3 { get; set; }
    public int I4 { get; set; }
    public int I5 { get; set; }
}